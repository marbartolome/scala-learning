import org.specs2.mutable._
import euler.Problems
import euler._

class EulerUnitTest extends Specification {

  "Javi tests" should {
    "test 1 return 1" in {
      Problems.problem1 must equalTo (1)
    }
    "return a list of elements sorted by price desc" in {
      val items = Seq(Item("1", 15.25), Item("2", 12.63), Item("3", 74.32), Item("4", 96.26))
      val expected = Seq(Item("4", 96.26),Item("3", 74.32), Item("1", 15.25), Item("2", 12.63) )
      Problems.sortByPrice(items) must equalTo(expected)
    }
    "return a list of category groups sorted by alphabet" in {
      val ItemCategories = Seq(ItemCategory("5", 92.26, "berberecho"),
    		  						ItemCategory("1", 15.25, "abracadabra"), 
    		  					ItemCategory("2", 12.63,"abracadabra"), 
    		  					ItemCategory("3", 74.32, "zarzaparrilla"), 
    		  					ItemCategory("4", 96.26, "berberecho"))
      val result = Problems.sortByCategory(ItemCategories)
      result.size  must equalTo(3)
      result.head must equalTo("abracadabra", Seq(Item("1", 15.25),Item("2", 12.63)))
      result.last must equalTo( "zarzaparrilla",Seq(Item("3", 74.32)))
    }
  }
}