package euler

object Problems {
	def problem1 = 1
	
	def sortByPrice(items: Seq[Item]): Seq[Item] = {
	  items.sortBy( item => item.price).reverse
	}
	def sortByCategory(items: Seq[ItemCategory]): Seq[(String, Seq[Item])] = {
	  items.groupBy( item => item.category)
		  .map( g => {
		    val group = g._1
		    val items = g._2.map(itemcat => Item(itemcat.name, itemcat.price))
		    ( group, items ) 
		  })
		  .toSeq  	  
		  .sortBy( _._1 )
	}
}


case class Item(name: String, price: Double)
case class ItemCategory (name: String, price: Double, category: String)